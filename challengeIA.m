function [ output_args ] = challengeIA( input_args )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    load Desktop/IA/trainData.mat
    p = trainSamples;
    clear trainSamples
    [n, m] = size(p);
    %t = trainLabels';
    t = zeros(10, 1);
    t(trainLabels(1)) = 1;
    for i = 2:m
        q = zeros(10, 1);
        q(trainLabels(i)) = 1;
        t = [t q];
    end
    
    net = patternnet([200, 200]);
    net = train(net,p,t);
    simulare = sim(net, p);
    [haha clase] = max(simulare);
    sum(clase == trainLabels')
    
    load Desktop/IA/testData-public.mat
    y = net(testSamples);
    classes = vec2ind(y);
    Id = [1:5000]';
    Prediction = classes';
    m = [Id Prediction];
    fid = fopen('test.csv', 'w') ;
    fprintf(fid, '%s,', 'Id') ;
    fprintf(fid, '%s\n', 'Prediction') ;
    fclose(fid) ;

    dlmwrite('test.csv', m, '-append') ;

end

